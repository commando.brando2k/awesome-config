-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
awful.autofocus = require("awful.autofocus")

-- Theme handling library
beautiful = require("beautiful")
-- Notification library
naughty = require("naughty")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ bg = beautiful.bg_urgent, fg = beautiful.fg_urgent,
                         timeout = 0,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Theme
local theme = awful.util.get_configuration_dir() .. "themes/default/theme.lua"
if not awful.util.file_readable(theme) then
    theme = awful.util.get_themes_dir() .. "default/theme.lua"
end
beautiful.init(theme)

-- {{{ Global config
config = require("config")

-- {{{ Autorun
require("autorun")

-- {{{ Menu
require("menu")

-- {{{ Signals
require("signals")

-- {{{ Keybindings
require("keys")

-- {{{ Mouse config
buttons = require("rat")

-- {{{ Wibox
require("wi")

-- Client rules
require("rules")

