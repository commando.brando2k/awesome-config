local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local config = {}

config.modkey = "Mod4"
config.terminal = "termite"
config.editor = os.getenv("EDITOR") or "vim"
config.editor_cmd = config.terminal .. " -e " .. config.editor
config.dmenu_pass = os.getenv("HOME") .. "/.local/bin/dmenu_pass"

-- Table of layouts to cover with awful.layout.inc, order matters.
config.layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}

-- Tag table
config.tags = { "➊:www", "➋:term", "➌:dev", "➍:other" }

local grad = {
    type = "linear",
    from = { 0, 0 },
    to = { 8, 0 },
    stops = {
        { 0, beautiful.bg_focus, },
        { 1, beautiful.bg_normal, },
    },  
}

local sep = wibox.widget {
    {
        text = "",
        widget = wibox.widget.textbox,
    },
    shape = gears.shape.rectangular_tag,
    shape_border_width = 2,
    shape_border_color = gears.color(grad),
    forced_width = 10,
    layout = wibox.container.background,
}

local battery_exists = false
local f = io.open("/sys/class/power_supply/BAT0", "r")
if f ~= nil then battery_exists = f:close() end

config.widgets = wibox.widget {
    wibox.widget.systray(),
    sep,
    require("widgets.volume"),
    sep,
    battery_exists and require("widgets.battery") or nil,
    battery_exists and sep or nil,
    require("widgets.clock"),
    sep,
    spacing = 4,
    layout = wibox.layout.fixed.horizontal,
}

return config

