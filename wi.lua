-- {{{ Wibox

local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local theme = require("beautiful")
local config = require("config")

-- Create a wibox for each screen and add it
local function set_wallpaper(s)
    if theme.wallpaper then
        local wallpaper = theme.wallpaper
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end
screen.connect_signal("property::geometry", set_wallpaper)

local function set_workspace(s)
    set_wallpaper(s)
    
    awful.tag(config.tags, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.promptbox = awful.widget.prompt()

    -- We need one layoutbox per screen.
    s.layoutbox = awful.widget.layoutbox(s)
    s.layoutbox:buttons(buttons.layoutbox)

    -- Create a taglist widget
    s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, buttons.taglist)

    -- Create a tasklist widget
    s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, buttons.tasklist)

    -- Create the wibox
    s.wibox = awful.wibar({ position = "top", screen = s })
    s.conkybox = awful.wibar({ position = "bottom", screen = s,
    				           ontop = false, width=1, height=10 })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(s.taglist)
    left_layout:add(s.promptbox)

    -- Widgets that are aligned to the right
    local right_layout = config.widgets
    right_layout:add(s.layoutbox)

    local wibox_layout = wibox.layout.align.horizontal()
    wibox_layout:set_left(left_layout)
    wibox_layout:set_middle(s.tasklist)
    wibox_layout:set_right(right_layout)

    s.wibox:set_widget(wibox_layout)

end
awful.screen.connect_for_each_screen(set_workspace)

-- }}}

