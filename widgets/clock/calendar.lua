-- calendar.lua
-- Displays a calendar popup under a widget

local string = {
    format = string.format,
    rep = string.rep,
}

local os = {
    date = os.date,
    time = os.time,
}

local awful = require("awful")
local gears = require("gears")
local theme = require("beautiful")
local wibox = require("wibox")

-- Theme
theme.cal_fg_normal = theme.cal_fg_normal or theme.fg_normal
theme.cal_fg_focus = theme.cal_fg_focus or theme.fg_focus
theme.cal_bg_normal = theme.cal_bg_normal or theme.bg_normal
theme.cal_font = theme.cal_font or theme.font
theme.cal_icon = theme.cal_icon or theme.awesome_icon

local function get_cal_month(offset)
    offset = offset or 0
    local cur_date = os.date("*t")
    local month = cur_date.month + offset

    local first_t = os.time{ year = cur_date.year, month = month, day = 0 }
    local last_t  = os.time{ year = cur_date.year, month = month + 1, day = 0 }
    local date    = os.date("*t", last_t)
    local prev    = os.date("*t", first_t)

    local this_month = (cur_date.month == date.month) and 
                       (cur_date.year == date.year)
    
    local fmt = '<span weight="bold" foreground="%s">%s</span>'
    local result = "Su Mo Tu We Th Fr Sa\n" --.. string.rep('   ', wstart)
    if prev.wday < 7 then
        for day = prev.day - prev.wday + 1, prev.day do
            result = string.format('%s<span foreground="%s">%2d </span>', 
                                    result, theme.cal_fg_prev, day )
        end
    end

    for day = 1, date.day do
        local last_in_week = (day + prev.wday) % 7 == 0
        local day_str = string.format('%2d', day)
        if day ~= date.day then
            day_str = day_str .. (last_in_week and "\n" or " ")
        end
        if this_month and day == cur_date.day then
            day_str = fmt:format(theme.cal_fg_focus, day_str)
        end
        result = result .. day_str
    end

    local header 
    if this_month then
        header = os.date("%a, %d %b %Y")
        header = fmt:format(theme.cal_fg_focus, header)
    else
        header = os.date("%B %Y", last_t)
    end

    return header, string.format('<span font="%s" foreground="%s">%s</span>',
                                 theme.cal_font, theme.cal_fg_normal, result)
end

local function update_geometry(self)
    self.screen = awful.screen.focused()
    local w_geo = mouse.current_widget_geometry

    local border = self.border_width * 2
    local _, header = self.header:get_preferred_size()
    local body_w, body_h = self.body:get_preferred_size()

    local geo = {
        x = (w_geo.x or self.x) - border + self.screen.workarea.x,
        y = self.screen.workarea.y, 
        width = body_w + border,
        height = header + body_h + border,
    }

    -- Don't go offscreen
    if geo.x + geo.width > self.screen.workarea.x + self.screen.workarea.width then 
        geo.x = self.screen.workarea.width - geo.width - border
    elseif geo.x < self.screen.workarea.x then
        geo.x = self.screen.workarea.x + border
    end

    return self:geometry(geo)
end

local function display(self)
    self.display_timer:stop()

    local header, body = get_cal_month(self.offset)
    self.header:set_markup(header)
    self.body:set_markup(body)
    self:update_geometry()
    self.visible = true
end

local function hide(self)
    self.display_timer:stop()
    self.reset_offset:again()
    self.visible = false
end

local function show(self, offset)
    if self.reset_offset.started then self.reset_offset:stop() end
    self.offset = self.offset + (offset or 0)

    if not self.visible and not self.display_timer.started then
        self.display_timer:start() 
    elseif self.visible then
        display(self)
    end
end

local function register(self, mywidget)
    mywidget:connect_signal("mouse::enter", 
    function(result)
        for k,v in pairs(result) do print(k,v) end
        self:show(0) 
    end)
    mywidget:connect_signal("mouse::leave", function() self:hide() end)

    mywidget:buttons(awful.util.table.join( awful.button({}, 1, function() 
                                                                    self:show(0) 
                                                                end),
                                            awful.button({}, 4, function()
                                                                    self:show(1)
                                                                end),
                                            awful.button({}, 5, function()
                                                                    self:show(-1)
                                                                end)))
    
end

local function new(widget)
    local header = wibox.widget.textbox('Some widget you are')
    header.align = 'center'
    local body = wibox.widget.textbox('Some widget you are')

    local cal = wibox {
        -- Theme
        fg = theme.cal_fg_normal,
        bg = theme.cal_bg_normal,
        border_width = theme.border_width or 1,
        border_color = theme.border_focus,

        -- Fallback position/size
        x = awful.screen.focused().workarea.width - 140,
        y = awful.screen.focused().workarea.y,
        width = 140,
        height = 100,

        visible = false,
        ontop = true,
        widget = wibox.widget {
            {
                {
                    header,
                    bgimage = theme.cal_icon,
                    layout = wibox.container.background,
                },
                body,
                mode = 'outside',
                layout = wibox.layout.align.vertical
            },
            margins = theme.border_width,
            layout = wibox.container.margin
        }
    }

    cal.offset          = 0
    cal.body            = body
    cal.header          = header
    cal.hide            = hide
    cal.show            = show
    cal.register        = register
    cal.update_geometry = update_geometry

    cal.reset_offset = gears.timer({ timeout = 5, single_shot = true })
    cal.reset_offset:connect_signal("timeout",
    function()
        cal.offset = 0
    end)

    cal.display_timer = gears.timer({ timeout = 2, single_shot = true })
    cal.display_timer:connect_signal("timeout",
    function()
        display(cal)
    end)

    if widget then register(cal, widget) end
    return cal
end

return setmetatable({}, { __call = function(_, ...) return new(...) end })

-- vim:set ts=4 sw=4 et:
