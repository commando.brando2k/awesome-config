-- Clock widget
--

local wibox = require("wibox")
local cal = require("widgets.clock.calendar")

local clock = wibox.widget {
    format = "%A %b %d %R",
    timeout = 60,
    widget = wibox.widget.textclock
}

cal(clock)

return clock
