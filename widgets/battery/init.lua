-- Battery widget
-- TODO: 
--  - notifications on ac state

local awful = require("awful")
local wibox = require("wibox")
local vicious = require("vicious")

local math = {
    floor = math.floor,
    ceil = math.ceil
}

battery = wibox.widget {
    {
        text = "",
        widget = wibox.widget.textbox,
    },
    layout = wibox.container.background
}

batt_text = ""
batt_t = awful.tooltip({
    objects = { battery },
    mode = "outside",
    timer_function = function() return batt_text end
})

vicious.register(battery, vicious.widgets.bat, function(widget, args)
    local low = 25
    local high = 75
    local color = beautiful.batt_fg_high
    local chg = args[2]
    local discharge = false

    local acad = args[1]
    if (acad == "+") then
        acad = "⏚"
        discharge = false
    else
        acad = "⌁"
        discharge = true
    end

    local step = 20
    local index = math.floor((chg / step) + .5)
    battery:set_bgimage(discharge and beautiful.icons.battery.discharging[index] or beautiful.icons.battery.charging[index])

    battery.widget:set_markup(string.format('<span size="smaller" color="%s">%03d</span>', color, chg))
    batt_text = string.format('[%s <span color="%s">%03d</span>%% %s',
    			      acad, color, chg,
                              args[3] ~= "N/A" and "(" .. args[3] .. "h remaining)]" or "]")
    return batt_text
end, 17, "BAT0")

return battery

-- vim:set ts=4 sw=4 et:
