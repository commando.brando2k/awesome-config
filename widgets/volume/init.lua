-- {{{ Volume widget

local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local util = require("util")
local pa = require("widgets.volume.pulseaudio")

local math = {
    floor = math.floor,
    ceil = math.ceil
}

local vol = wibox.widget {
    {
        value   = pa:volume(),
        border_width = 1,
        border_color = beautiful.border_focus,
        widget  = wibox.widget.progressbar,
    },
    bg_normal   = beautiful.vol_bg_normal,
    fg_normal   = beautiful.vol_fg_normal,
    bg_mute     = beautiful.vol_bg_mute,
    fg_mute     = beautiful.vol_fg_mute,
    height      = beautiful.vol_height,
    width       = beautiful.vol_width,
    layout      = wibox.container.constraint,
}

function vol.update(volume, muted)
    
    local color = util.gradient(vol.fg_mute, vol.fg_normal, 0, 1, volume)
    if (muted == "no") then
        vol.widget:set_color(color)
        vol.widget:set_background_color(vol.bg_normal)
    else
        vol.widget:set_color(vol.fg_mute)
        vol.widget:set_background_color(vol.bg_mute)
    end

    vol.widget:set_value(volume)

    local text = '<span font_variant="smallcaps" underline="single">%s</span>\n' ..
                 '<span color="%s">Volume:\t%3d%%</span>\n' ..
                 '<span color="%s">Muted:\t%4s</span>'
    
    text = text:format(" Pulseaudio ",
                       color, math.floor(volume * 100),
                       muted == "yes" and vol.fg_mute or vol.fg_normal, muted)
    vol.text = text
    --vol.text = tostring(pa)
end

vol_t = awful.tooltip({
    objects = { vol },
    mode = "outside",
    timer_function = function() return vol.text end
})

-- Update widget with volume changes
pa:connect_signal(pa.signals.update,
function(obj, val)
    vol.update(pa:volume(), pa:muted())
end)

vol:connect_signal("volume::update", 
function(obj, val)
    if (val == "Raise") then
        pa:raise()
    elseif (val == "Lower") then
        pa:lower()
    elseif (val == "Toggle") then
        pa:toggle()
    end

end)

vol:connect_signal("mouse::enter",
function(c)
    vol.update(pa:volume(), pa:muted())
end)

vol:buttons(awful.util.table.join(
    awful.button({ }, 1, function() pa:toggle() end),
    awful.button({ }, 3, function() pa:mixer() end),
    awful.button({ }, 4, function() pa:raise() end),
    awful.button({ }, 5, function() pa:lower() end)
))

return vol

-- vim:set ts=4 sw=4 et:
