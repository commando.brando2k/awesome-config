-- pulseaudio.lua
-- Awesome volume (pulseaudio) management

local string = {
    format = string.format,
    match = string.match,
}

local math = {
    floor = math.floor,
}

local setmetatable = setmetatable
local tonumber = tonumber
local tostring = tostring

local awful = require("awful")
local util = require("util")
local gears = require("gears")
local debug = require("gears.debug")

local awesome = awesome

local signals = {
    err             = "pulseaudio::error",
    mute            = "pulseaudio::mute",
    request_update  = "pulseaudio::request_update",
    update          = "pulseaudio::update",
    volume          = "pulseaudio::volume",
}

local pacmd = "/usr/bin/pacmd"
local mixer = { 
    startup_id = "pulseaudio-volume-mixer", 
    cmd = {
        "/usr/bin/env",
        "DESKTOP_STARTUP_ID=pulseaudio-volume-mixer",
        "/usr/bin/pavucontrol",
    },
}

local Pulseaudio = gears.object{}

function Pulseaudio:exec(cmd)
    if type(cmd) ~= "string" then return "Command must be string" end

    local pid = awesome.spawn(cmd, false, false, false, false, 
        function(exit_reason, exit_code) 
            if exit_reason ~= "exit" then
                local msg = '%s failed.\nExit Reason: %s\nExit Code: %d'
                self:emit_signal(signals.err, msg:format(cmd, exit_reason, exit_code))
            end
            self:emit_signal(signals.request_update)
        end)

    if type(pid) == "string" then
        self:emit_signal(signals.err, "Error running " .. cmd .. ":\n" .. pid)
    end
    return pid
end

-- Raise the volume of the current sink
function Pulseaudio:raise(inc)
    inc = inc or 0.05
    self:emit_signal(signals.volume, self._volume + inc)
end

-- Lower the volume of the current sink
function Pulseaudio:lower(dec)
    dec = dec or 0.05
    self:emit_signal(signals.volume, self._volume - dec)
end

-- Toggle the muted state of the current sink
function Pulseaudio:toggle()
    local muted = self._mute == "no"
    self:emit_signal(signals.mute, muted)
end

-- Get the volume of the current sink
function Pulseaudio:volume()
    return self._volume
end

-- Get the current mute state
function Pulseaudio:muted()
    return self._mute
end

-- Open or close a mixer client
function Pulseaudio:mixer()
    local function matcher(c)
        return awful.rules.match(c, { startup_id = mixer.startup_id})
    end

    util.run_or_die(mixer.cmd, matcher)
end

--- Signal handlers

-- pulseaudio::volume
local function on_signal_volume(obj, vol)
    if type(vol) ~= "number" then return end

    if (vol > 1) then vol = 1.0 end
    if (vol < 0) then vol = 0.0 end
    vol = math.floor(vol * 0x10000)

    local cmd = string.format("%s set-sink-volume %s %i", pacmd, obj._sink, vol)
    local pid = obj:exec(cmd) 
end

-- pulseaudio::mute
local function on_signal_mute(obj, mute)
    local cmd = string.format("%s set-sink-mute %s %s", pacmd, obj._sink, mute)
    local pid = obj:exec(cmd) 
end

-- pulseaudio::request_update
local function on_signal_request_update(obj)
    local cmd = pacmd .. " dump"
    local function callback(stdout, stderr, exit_reason, exit_code)
        if exit_code ~= 0 or type(stdout) ~= "string" then
            local msg = 'Error processing %s\nExited with code %d\nREASON: %s'
            obj:emit_signal(signals.err, msg:format(cmd, exit_code, exit_reason))
            return exit_code
        end

        local sink = stdout:match("set%-default%-sink (%g+)") or "unavailable"
        if sink then
            obj._sink = sink
        end
        sink = util.escape_string(sink)

        local volume = stdout:match("set%-sink%-volume " .. sink .. " (%g+)") or 0 
        if volume then
            obj._volume = tonumber(volume) / 0x10000
        end

        local muted = stdout:match("set%-sink%-mute " .. sink .. " (%g+)") or "unknown" 
        if muted then
            obj._mute = muted
        end

        obj:emit_signal(signals.update)
        obj._timer_refresh:again()
    end

    local pid = awful.spawn.easy_async(cmd, callback)
    if type(pid) == "string" then
        obj:emit_signal(signals.err, "Error running " .. cmd .. ":\n" .. pid)
    end
end

-- pulseaudio::error
local function on_signal_error(obj, err)
    if type(err) ~= "string" then
        err = tostring(err)
    end
    debug.print_error(err)
    awesome.emit_signal("debug::error", err)
end

-- pulseaudio::update
local function on_signal_update(obj, err)
    -- see: https://github.com/pavouk/lgi/issues/157
    collectgarbage()
end

function Pulseaudio:new(o)
    local o = o or {}
    setmetatable(o, self)

    self.__index = self
    self.__name = require("gears.object").modulename(2) or "Pulseaudio"
    self.__tostring = 
        function(obj)
            local text = '%s\nVolume: %3d%%\nMuted: %5s\nSink: %s'
            local vol = math.floor(obj._volume * 100)
            return text:format(obj.__name, vol, obj._mute, obj._sink)
        end

    self.signals = signals
    self._sink = ""
    self._volume = 0
    self._mute = false

    self._timer_refresh = gears.timer.start_new(3.0, 
        function() 
            self:emit_signal(signals.request_update) 
            return true 
        end)

    self:connect_signal(signals.err, on_signal_error)
    self:connect_signal(signals.mute, on_signal_mute)
    self:connect_signal(signals.request_update, on_signal_request_update)
    self:connect_signal(signals.update, on_signal_update)
    self:connect_signal(signals.volume, on_signal_volume)

    self:emit_signal(signals.request_update)
    return o
end

return Pulseaudio:new()

-- vim:set ts=4 sw=4 ft=lua et:
