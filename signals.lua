local awful = require("awful")
awful.layout = require("awful.layout")
local wibox = require("wibox")

-- {{{ Signals

-- Signal function to execute when a new client appears.
client.connect_signal("manage", 
function (c)
    if awesome.startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.connect_signal("untagged",
function(c, t)
    if c.name == "Steam Login" then
        -- Bitch, please
        if t.name == config.tags[4] then
            c:move_to_tag(t)
        end
    end
end)

-- Enable sloppy focus
client.connect_signal("mouse::enter", 
function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", 
function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- {{{ Fullscreen display management
-- Turn off dpms when client is fullscreen
client.connect_signal("property::fullscreen", 
function(c)
if(c.fullscreen) then
        awful.util.spawn("xset s off")
        awful.util.spawn("xset -dpms")
else
        awful.util.spawn("xset s on")
    awful.util.spawn("xset +dpms")
end
end)

client.connect_signal("unmanage",
function(c)
    if(c.fullscreen) then
        awful.util.spawn("xset s on")
        awful.util.spawn("xset +dpms")
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

