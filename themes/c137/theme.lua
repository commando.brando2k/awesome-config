-------------------------------
--  "c137" awesome theme     --
--  Author: B.A Parker       --
-------------------------------

local awful = require("awful")
local gears = require("gears")
local xresources = require("beautiful").xresources
local xrdb = xresources.get_current_theme()
local dpi = xresources.apply_dpi

local theme = dofile(awful.util.get_themes_dir() .. "default/theme.lua")
local theme_assets = dofile(awful.util.get_themes_dir() .. "xresources/assets.lua")

theme.confdir = awful.util.get_configuration_dir()
theme.wallpaper = theme.confdir .. "themes/default/background.png"

theme.font      = "Noto Sans 8"
theme.font_mono = "Noto Mono 8"

theme.bg_normal   = xrdb.background
theme.bg_focus    = xrdb.color4
theme.bg_urgent   = xrdb.color1
theme.bg_minimize = xrdb.color0
theme.bg_systray  = theme.bg_normal

theme.fg_normal   = xrdb.foreground
theme.fg_focus    = xrdb.color6
theme.fg_urgent   = theme.fg_normal
theme.fg_minimize = theme.fg_normal

-- Borders
theme.useless_gap   = dpi(1)
theme.border_width  = dpi(2)
theme.border_normal = xrdb.color0
theme.border_focus  = theme.bg_focus
theme.border_marked = xrdb.color10

-- Menu
theme.menu_height = dpi(16)
theme.menu_width  = dpi(128)

-- Progress bar widgets
theme.progressbar_bg = theme.bg_normal
theme.progressbar_fg = theme.fg_normal
theme.progressbar_border_color = theme.border_normal
theme.progressbar_margins = dpi(4)

-- Notifications
theme.notification_font = theme.font_mono
theme.notification_fg = theme.fg_normal
theme.notification_bg = theme.bg_normal
theme.notification_border_width = theme.border_width
theme.notification_border_color = theme.border_focus

-- Tooltip
theme.tooltip_font = theme.font_mono
theme.tooltip_bg   = theme.bg_normal
theme.tooltip_fg   = theme.fg_normal
theme.tooltip_border_color = theme.border_focus
theme.tooltip_border_width = dpi(1) --theme.border_width

-- Battery 
theme.batt_bg_full	= theme.bg_normal
theme.batt_fg_full	= theme.fg_normal
theme.batt_bg_high	= theme.bg_focus
theme.batt_fg_high	= theme.fg_focus
theme.batt_bg_low	= theme.bg_urgent
theme.batt_fg_low	= theme.fg_urgent
theme.batt_border	= theme.border_normal

-- Calendar widget
theme.cal_font      = theme.notification_font
theme.cal_icon      = gears.color.recolor_image(theme.confdir .. "/icons/cal.png", theme.bg_focus)
theme.cal_fg_normal = theme.notification_fg
theme.cal_fg_focus  = theme.bg_focus
theme.cal_fg_prev   = xrdb.color8

-- Volume widget
theme.vol_height    = dpi(8)
theme.vol_width     = dpi(30)
theme.vol_bg_normal = theme.bg_normal
theme.vol_fg_normal = theme.fg_normal
theme.vol_bg_mute   = theme.bg_urgent
theme.vol_fg_mute   = theme.bg_urgent

-- {{{ Icons
theme.icon_theme = "Faenza"
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height,
                                               theme.bg_focus,
                                               theme.fg_focus)
theme = theme_assets.recolor_titlebar_normal(theme, theme.fg_normal)
theme = theme_assets.recolor_titlebar_focus(theme, theme.fg_focus)
theme = theme_assets.recolor_layout(theme, theme.fg_normal)
local submenu_icon      = awful.util.get_themes_dir() .. "default/submenu.png"
theme.menu_submenu_icon = gears.color.recolor_image(submenu_icon, theme.bg_focus)
theme.menu_submenu_icon = gears.color.recolor_image(submenu_icon, theme.bg_normal)
theme.menu_submenu_icon = submenu_icon

-- Taglist
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
                               taglist_square_size, 
                               theme.fg_normal
                            )
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
                                 taglist_square_size, 
                                 theme.fg_normal
                              )

theme.icons = {}
theme.icons.path = "/usr/share/icons/" .. theme.icon_theme .. "/"
theme.icons.battery = {}
theme.icons.battery["charged"] = theme.icons.path .. "status/scalable/gpm-battery-charged.svg"
theme.icons.battery.charging = {}
theme.icons.battery.charging[0] = theme.icons.path .. "status/scalable/gpm-battery-000-charging.svg"
theme.icons.battery.charging[1] = theme.icons.path .. "status/scalable/gpm-battery-020-charging.svg"
theme.icons.battery.charging[2] = theme.icons.path .. "status/scalable/gpm-battery-040-charging.svg"
theme.icons.battery.charging[3] = theme.icons.path .. "status/scalable/gpm-battery-060-charging.svg"
theme.icons.battery.charging[4] = theme.icons.path .. "status/scalable/gpm-battery-080-charging.svg"
theme.icons.battery.charging[5] = theme.icons.path .. "status/scalable/gpm-battery-100-charging.svg"
theme.icons.battery.discharging = {}
theme.icons.battery.discharging[0] = theme.icons.path .. "status/scalable/gpm-battery-000.svg"
theme.icons.battery.discharging[1] = theme.icons.path .. "status/scalable/gpm-battery-020.svg"
theme.icons.battery.discharging[2] = theme.icons.path .. "status/scalable/gpm-battery-040.svg"
theme.icons.battery.discharging[3] = theme.icons.path .. "status/scalable/gpm-battery-060.svg"
theme.icons.battery.discharging[4] = theme.icons.path .. "status/scalable/gpm-battery-080.svg"
theme.icons.battery.discharging[5] = theme.icons.path .. "status/scalable/gpm-battery-100.svg"

-- Widget icons
theme.widget_cpu    = theme.confdir .. "/icons/cpu.png"
theme.widget_mem    = theme.confdir .. "/icons/mem.png"
theme.widget_fs     = theme.confdir .. "/icons/disk.png"
theme.widget_net    = theme.confdir .. "/icons/down.png"
theme.widget_netup  = theme.confdir .. "/icons/up.png"
theme.widget_wifi   = theme.confdir .. "/icons/wifi.png"
theme.widget_mail   = theme.confdir .. "/icons/mail.png"
theme.widget_vol    = theme.confdir .. "/icons/vol.png"
theme.widget_org    = theme.confdir .. "/icons/cal.png"
theme.widget_date   = theme.confdir .. "/icons/time.png"
theme.widget_crypto = theme.confdir .. "/icons/crypto.png"
theme.widget_sep    = theme.confdir .. "/icons/separator.png"

-- }}}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
