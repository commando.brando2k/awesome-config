-------------------------------
--  "Branzen" awesome theme  --
--  Author: Brandon Parker   --
--  Modified zenburn theme   --
-------------------------------

local awful = require("awful")

-- {{{ Main
theme = {}
theme.confdir = awful.util.get_configuration_dir()
theme.wallpaper = theme.confdir .. "themes/default/background.png"
-- }}}

-- {{{ Styles
theme.font      = "droid sans 8"

-- {{{ Colors
theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = "#F0DFAF"
theme.fg_urgent  = "#CC9393"
theme.bg_normal  = "#3F3F3Faa"
theme.bg_focus   = "#1E2320aa"
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.border_width  = 1
theme.border_normal = "#3F3F3F"
theme.border_focus  = "#1797d1"
theme.border_marked = "#CC9393"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = 15
theme.menu_width  = 128
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = "/usr/share/awesome/themes/zenburn/taglist/squarefz.png"
theme.taglist_squares_unsel = "/usr/share/awesome/themes/zenburn/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Battery 
theme.batt_bg_full	= theme.bg_normal
theme.batt_fg_full	= theme.fg_normal
theme.batt_bg_high	= theme.bg_focus
theme.batt_fg_high	= theme.fg_focus
theme.batt_bg_low	= theme.bg_urgent
theme.batt_fg_low	= theme.fg_urgent
theme.batt_border	= "#00ff00"

theme.icons = {}
theme.icons.path = "/usr/share/icons/Faenza/"
theme.icons.battery = {}
theme.icons.battery["charged"] = theme.icons.path .. "status/scalable/gpm-battery-charged.svg"
theme.icons.battery.charging = {}
theme.icons.battery.charging[0] = theme.icons.path .. "status/scalable/gpm-battery-000-charging.svg"
theme.icons.battery.charging[1] = theme.icons.path .. "status/scalable/gpm-battery-020-charging.svg"
theme.icons.battery.charging[2] = theme.icons.path .. "status/scalable/gpm-battery-040-charging.svg"
theme.icons.battery.charging[3] = theme.icons.path .. "status/scalable/gpm-battery-060-charging.svg"
theme.icons.battery.charging[4] = theme.icons.path .. "status/scalable/gpm-battery-080-charging.svg"
theme.icons.battery.charging[5] = theme.icons.path .. "status/scalable/gpm-battery-100-charging.svg"
theme.icons.battery.discharging = {}
theme.icons.battery.discharging[0] = theme.icons.path .. "status/scalable/gpm-battery-000.svg"
theme.icons.battery.discharging[1] = theme.icons.path .. "status/scalable/gpm-battery-020.svg"
theme.icons.battery.discharging[2] = theme.icons.path .. "status/scalable/gpm-battery-040.svg"
theme.icons.battery.discharging[3] = theme.icons.path .. "status/scalable/gpm-battery-060.svg"
theme.icons.battery.discharging[4] = theme.icons.path .. "status/scalable/gpm-battery-080.svg"
theme.icons.battery.discharging[5] = theme.icons.path .. "status/scalable/gpm-battery-100.svg"

-- }}}

-- {{{ Misc
theme.awesome_icon           = theme.confdir .. "/themes/default/awesome-icon.png"
theme.menu_submenu_icon      = theme.confdir .. "/themes/default/submenu.png"
theme.icon_theme = "Faenza"
-- }}}

-- {{{ Layout
theme.layout_tile       = "/usr/share/awesome/themes/zenburn/layouts/tile.png"
theme.layout_tileleft   = "/usr/share/awesome/themes/zenburn/layouts/tileleft.png"
theme.layout_tilebottom = "/usr/share/awesome/themes/zenburn/layouts/tilebottom.png"
theme.layout_tiletop    = "/usr/share/awesome/themes/zenburn/layouts/tiletop.png"
theme.layout_fairv      = "/usr/share/awesome/themes/zenburn/layouts/fairv.png"
theme.layout_fairh      = "/usr/share/awesome/themes/zenburn/layouts/fairh.png"
theme.layout_spiral     = "/usr/share/awesome/themes/zenburn/layouts/spiral.png"
theme.layout_dwindle    = "/usr/share/awesome/themes/zenburn/layouts/dwindle.png"
theme.layout_max        = "/usr/share/awesome/themes/zenburn/layouts/max.png"
theme.layout_fullscreen = "/usr/share/awesome/themes/zenburn/layouts/fullscreen.png"
theme.layout_magnifier  = "/usr/share/awesome/themes/zenburn/layouts/magnifier.png"
theme.layout_floating   = "/usr/share/awesome/themes/zenburn/layouts/floating.png"
-- }}}

-- {{{ Widget icons
theme.widget_cpu    = theme.confdir .. "/icons/cpu.png"
theme.widget_mem    = theme.confdir .. "/icons/mem.png"
theme.widget_fs     = theme.confdir .. "/icons/disk.png"
theme.widget_net    = theme.confdir .. "/icons/down.png"
theme.widget_netup  = theme.confdir .. "/icons/up.png"
theme.widget_wifi   = theme.confdir .. "/icons/wifi.png"
theme.widget_mail   = theme.confdir .. "/icons/mail.png"
theme.widget_vol    = theme.confdir .. "/icons/vol.png"
theme.widget_org    = theme.confdir .. "/icons/cal.png"
theme.widget_date   = theme.confdir .. "/icons/time.png"
theme.widget_crypto = theme.confdir .. "/icons/crypto.png"
theme.widget_sep    = theme.confdir .. "/icons/separator.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = "/usr/share/awesome/themes/zenburn/titlebar/close_focus.png"
theme.titlebar_close_button_normal = "/usr/share/awesome/themes/zenburn/titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = "/usr/share/awesome/themes/zenburn/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = "/usr/share/awesome/themes/zenburn/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = "/usr/share/awesome/themes/zenburn/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = "/usr/share/awesome/themes/zenburn/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = "/usr/share/awesome/themes/zenburn/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = "/usr/share/awesome/themes/zenburn/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = "/usr/share/awesome/themes/zenburn/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = "/usr/share/awesome/themes/zenburn/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = "/usr/share/awesome/themes/zenburn/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = "/usr/share/awesome/themes/zenburn/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = "/usr/share/awesome/themes/zenburn/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = "/usr/share/awesome/themes/zenburn/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = "/usr/share/awesome/themes/zenburn/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = "/usr/share/awesome/themes/zenburn/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = "/usr/share/awesome/themes/zenburn/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = "/usr/share/awesome/themes/zenburn/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
