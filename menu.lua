local awful = require("awful")
local theme = require("beautiful")

local os = os

local menubar = require("menubar")
local icon_theme = require("menubar.icon_theme")

restart_cmd = "zenity --question --title 'Reboot System' \
               --window-icon=warning --text 'Reboot?' --cancel-label 'Not now' \
               --ok-label 'Reboot' && /usr/bin/systemctl reboot"
suspend_cmd = "zenity --question --title 'Suspend System' \
               --window-icon=warning --text 'Suspend system?' --cancel-label 'Never mind' \
               --ok-label 'Suspend now' && systemctl suspend"
poweroff_cmd = "zenity --question --title 'Power Off System' \
               --window-icon=warning --text 'Shut down?' --cancel-label 'Not now' \
               --ok-label 'Poweroff' && systemctl poweroff"

menubar.cache_entries = true
menubar.show_categories = true
menubar.utils.terminal = config.terminal
menubar.all_menu_dirs = {
    os.getenv("XDG_DATA_HOME") .. '/applications',
    '/usr/share/applications',
}
table.insert(menubar.menu_gen.all_categories, { app_type = "Archlinux", 
						name = "Arch Linux",
						icon_name = "arch-logo",
						use = true })
table.insert(menubar.menu_gen.all_categories, { app_type = "Application", 
						name = "Other",
						icon_name = "applications-other",
						use = true })

-- {{{ Menu
-- Create a laucher widget and a main menu
local menu_items = {}
local awesome_menu = {
   { "manual", 
     config.terminal .. " -e man awesome", 
     icon_theme():find_icon_path('help') },
   { "edit config", 
     config.editor_cmd .. " " .. awesome.conffile,
     icon_theme():find_icon_path('accessories-text-editor') },
   { "restart", 
     awesome.restart, 
     icon_theme():find_icon_path('system-reboot') },
   { "quit", 
     function() awesome.quit() end, 
     icon_theme():find_icon_path('system-shutdown') },
}
table.insert(menu_items, {"awesome", awesome_menu, theme.awesome_icon})

local sys_menu = { 
    { "Lock Screen", 
      "/usr/bin/systemctl --user restart screen-lock.service", 
      icon_theme():find_icon_path("system-lock-screen")},
    { "Logout", 
      function() awesome.quit() end, 
      icon_theme():find_icon_path("system-log-out")},
    { "Suspend",
      suspend_cmd,
      icon_theme():find_icon_path("system-suspend")},
    { "Reboot", 
      restart_cmd,
      icon_theme():find_icon_path("system-reboot")},
    { "Shutdown",
      poweroff_cmd,
      icon_theme():find_icon_path("system-shutdown")},
}
table.insert(menu_items, {"System", sys_menu, icon_theme():find_icon_path('applications-system')})

client.connect_signal("menubar::show",
function(c)
    menubar.show(c.screen)
end)

client.connect_signal("menubar::refresh",
function(c)
    menubar.refresh(c.screen)
    menubar.show(c.screen)
end)

main_menu = awful.menu.new({ items = menu_items })
mylauncher = awful.widget.launcher({ image = theme.awesome_icon,
                                     menu = main_menu })
-- }}}

