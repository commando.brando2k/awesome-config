[![License WTFPL](https://img.shields.io/badge/License-wtfpl-lightgrey.svg)](http://www.wtfpl.net/)

# awesome-config #

Just my personal awesome wm config. Very out-of-date.


### License ###

The files within this repository are licensed under the WTFPLv2 unless 
otherwise stated.

Copyright © 2017 B.A. Parker \<commando.brando2k@gmail.com\>
This work is free. You can redistribute it and/or modify it under the terms of 
the Do What The Fuck You Want To Public License, Version 2, as published by Sam 
Hocevar. See http://www.wtfpl.net/ for more details.
