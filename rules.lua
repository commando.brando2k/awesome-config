local awful = require("awful")
awful.rules = require("awful.rules")

-- {{{ Rules

local placement = awful.placement.no_overlap + awful.placement.no_offscreen
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { 
          border_width = beautiful.border_width,
          border_color = beautiful.border_normal,
          focus = awful.client.focus.filter,
          raise = true,
          keys = clientkeys,
          buttons = buttons.client,
          screen = awful.screen.preferred,
          placement = placement
      }
    },
    { 
        rule_any = { 
            class = { "Chromium", "qutebrowser"} 
        },
        properties = { 
            screen = 1,
            tag = config.tags[1],
            switchtotag = true,
            callback = function(c) awful.layout.set(awful.layout.suit.magnifier) end,
            maximized_vertical = true,
            maximized_horizontal = true 
        } 
    },
    { 
        rule_any = { class = "Eclipse" },
        properties = { 
            screen = 1,
            tag = config.tags[3],
            switchtotag = true,
            maximized_vertical = false,
            maximized_horizontal = false 
        } 
    },
    { 
        rule_any = { 
            class = { "URxvt", "Termite" }
        },
        properties = { 
            screen = screen:count() > 1 and 2 or 1,
            tag = config.tags[2],
      		switchtotag = true,
		    floating = true,
        } 
    },
    { 
        rule_any = { 
            class = { "Steam", "Steam.exe", "Wine" } 
        },
        exept_any = { 
            name = { 
                "Steam Guard - Computer Authorization Required",
                "Steam Login",
            } 
        },
        properties = { 
            screen = screen.count() > 1 and 2 or 1,
            tag = config.tags[4],
      		border_width = 0,
		    floating = true,
            switchtotag = false,
            placement = placement + awful.placement.centered,
        } 
    },
    { 
        rule_any = { 
            name = { 
                "Steam Guard - Computer Authorization Required",
                "Steam Login",
            } 
        },
        properties = { 
            screen = screen.count() > 1 and 2 or 1,
            tag = config.tags[4],
      		border_width = 0,
		    floating = true,
            switchtotag = false,
            callback = function(c)
                local placement =  awful.placement.centered
                return c:geometry(placement(c, { honor_workarea = true }))
            end
        } 
    },
    { 
        rule_any = { 
            class = { "pinentry" } 
        },
        properties = { 
            floating = true, 
            titlebars_enabled = true, 
        } 
    },
    {
        rule = {
            startup_id = "pulseaudio-volume-mixer"
        },
        properties = {
            border_width = 1,
            floating = true,
            titlebars_enabled = false,
            callback = function(c)
                local placement = awful.placement.scale + awful.placement.top_right
                return c:geometry(placement(c, { honor_workarea = true, to_percent = 0.5 }))
            end
        }
    },
    {
        rule = {
            type = "dialog"
        },
        properties = {
            placement = placement + awful.placement.centered,
            titlebars_enabled = true 
        }
    },
}

-- }}}

