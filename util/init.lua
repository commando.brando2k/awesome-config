local string = {
    format = string.format,
    len = string.len
}
local tostring = tostring
local tonumber = tonumber
local os = os

local awful = require("awful")
local naughty = require("naughty")

local util = {}

util.notify_timeout = 10
util.notify_id = 0

function util.print(...)
    local str = string.format(...)
    util.notify_id = naughty.notify({text = str, 
                                     timeout = util.notify_timeout,
                                     replaces_id = util.notify_id}).id
end

function util.printf(str)
    local txt = ""
    for i=1, #str do
        txt = txt .. tostring(str[i]) .. " | "
    end
    naughty.notify({ text = txt, timeout = 0 })
end

function util.pop_spaces(s1, s2, maxsize)
    local sps = ""
    for i = 1, maxsize - string.len(s1) - string.len(s2) do
        sps = sps .. " "
    end
    return s1 .. sps .. s2
end

function util.escape_string(text)
    return text and text:gsub("[%.%-]", {["."] = "%.", ["-"] = "%-"})
end

--{{{ Run a process if it isn't running already
function util.run_once(prg, arg_str, pname, screen)
	if not prg then
		do return nil end
	end

	if not pname then
		pname = prg
	end

    local cmd = "pgrep -u $USER -x " .. pname .. " || " .. prg
    if arg_str then
        cmd = cmd .. " " .. arg_str
    end

    awful.spawn.with_shell(cmd)
end

-- Kill a running program if it exists, or spawn it
function util.run_or_die(cmd, matcher)

    if not type(cmd) == "string" or not type(cmd) == "table" then
        return nil
    end

    local c = nil
    if type(matcher) == "function" then
        c = awful.client.iterate(matcher)()
    end
        
    if c then
        c:kill()
    else
        awful.spawn(cmd)
    end
end

--{{{ Caclulates a gradient color value
-- From http://awesome.naquadah.org/wiki/Gradient
function util.gradient(color, to_color, min, max, value)
    local function color2dec(c)
        return tonumber(c:sub(2,3),16), tonumber(c:sub(4,5),16), tonumber(c:sub(6,7),16)
    end

    local factor = 0
    if (value >= max) then
        factor = 1
    elseif (value > min) then
        factor = (value - min) / (max - min)
    end
    
    local red, green, blue = color2dec(color)
    local to_red, to_green, to_blue = color2dec(to_color)

    red = math.floor(red + (factor * (to_red - red)))
    green = math.floor(green + (factor * (to_green - green)))
    blue = math.floor(blue + (factor * (to_blue - blue)))

    return string.format("#%02x%02x%02x", red, green, blue)
end
--}}}

local function format_color(widget, args)
    local text

    local vol = args[1]
    if 1 then 
        local color = gradient("#AECF96", "#FF5656", 50, 100, args[1])
        args[1] = string.format("<span color='%s'>%s</span>", color, args[1])
        vol = args[1]
        return vol
    end

    for i=1,#args do
        if args[1] > 50 then
            local color = gradient("#AECF96", "#FF5656", 50, 100, args[i])
            args[i] = string.format("<span color='%s'>%s</span>", color, args[i])
        end

        if i > 2 then 
            text = text .. "/" .. argx[i] .. "%"
        else 
            text = args[i] .. "%" 
        end
    end

    return text
end

return util

-- vim:set ts=4 sw=4 et:
