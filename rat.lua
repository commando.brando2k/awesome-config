local awful = require("awful")
local config = require("config")

-- {{{ Mouse bindings

local modkey = config.modkey
local buttons = {}
buttons.client = {}
buttons.global = {}
buttons.layoutbox = {}
buttons.taglist = {}
buttons.tasklist = {}

buttons.global = awful.util.table.join(
    awful.button({ }, 3, function () main_menu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
)
root.buttons(buttons.global)

-- Client buttons
buttons.client = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

buttons.layoutbox = awful.util.table.join(
                    awful.button({ }, 1, function () awful.layout.inc(1) end),
                    awful.button({ }, 3, function () awful.layout.inc(-1) end),
                    awful.button({ }, 4, function () awful.layout.inc(1) end),
                    awful.button({ }, 5, function () awful.layout.inc(-1) end)
                    )

buttons.taglist = awful.util.table.join(
                  awful.button({ }, 1, awful.tag.viewonly),
                  awful.button({ modkey }, 1, awful.client.movetotag),
                  awful.button({ }, 3, awful.tag.viewtoggle),
                  awful.button({ modkey }, 3, awful.client.toggletag),
                  awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                  awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                  )

buttons.tasklist = awful.util.table.join(
                   awful.button({ }, 1, function (c)
                                            if c == client.focus then
                                                c.minimized = true
                                            else
                                                -- Without this, the following
                                                -- :isvisible() makes no sense
                                                c.minimized = false
                                                if not c:isvisible() then
                                                    awful.tag.viewonly(c:tags()[1])
                                                end
                                                -- This will also un-minimize
                                                -- the client, if needed
                                                client.focus = c
                                                c:raise()
                                            end
                                        end),
                   awful.button({ }, 3, function ()
                                            if instance then
                                                instance:hide()
                                                instance = nil
                                            else
                                                instance = awful.menu.clients({ width=250 })
                                            end
                                        end),
                   awful.button({ }, 4, function ()
                                            awful.client.focus.byidx(1)
                                            if client.focus then client.focus:raise() end
                                        end),
                   awful.button({ }, 5, function ()
                                            awful.client.focus.byidx(-1)
                                            if client.focus then client.focus:raise() end
                                        end))

return buttons
-- }}}

